package in.hocg;

import in.hocg.pay.alipay.AliPayContext;
import in.hocg.pay.alipay.AliPayService;
import in.hocg.pay.alipay.params.AliPayTradePay;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        /**
         * 构建参数
         */
        AliPayService service = AliPayContext.ctx()
                .app_auth_token("")
                .app_id("")
                .charset("")
                .notify_url("")
                .method("")
                .timestamp("")
                .version("")
                .sign_type("")
                .biz_content("")
                .sign("")
                .getService();


        /**
         * 构建请求
         */
        AliPayTradePay aliPayTradePay = AliPayTradePay.builder().build();
        String response = service.request(aliPayTradePay);

//        aliPayService.route();
    }
}
