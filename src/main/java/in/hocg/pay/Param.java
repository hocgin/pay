package in.hocg.pay;

import okhttp3.*;

public interface Param {
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    RequestBody body();

    String url(boolean sandbox);
}
