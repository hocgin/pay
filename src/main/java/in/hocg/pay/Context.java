package in.hocg.pay;

import lombok.Data;
import lombok.experimental.Accessors;
import okhttp3.internal.http.HttpMethod;

/**
 * 存放全局属性
 */
@Data
@Accessors(fluent = true)
public abstract class Context<C extends Context, S extends Service> {
    private boolean sandbox = false;
    private boolean log = false;
    private Format format = Format.JSON;
    private String httpMethod = "POST";

    public C useSandbox() {
        this.sandbox = true;
        return ((C) this);
    }

    public C log() {
        this.log = true;
        return ((C) this);
    }

    public C json() {
        this.format = Format.JSON;
        return ((C) this);
    }

    public C xml() {
        this.format = Format.XML;
        return ((C) this);
    }

    public C post() {
        this.httpMethod = "POST";
        return ((C) this);
    }

    public C get() {
        this.httpMethod = "GET";
        return ((C) this);
    }

    public abstract S getService();
}
