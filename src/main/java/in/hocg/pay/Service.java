package in.hocg.pay;

import okhttp3.*;

import java.io.IOException;

public abstract class Service<C extends Context> {
    private static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient();
    private C context;

    public Service(C context) {
        this.context = context;
    }

    protected abstract Request.Builder wrap(Request.Builder builder);

    protected Response call(Param param) throws IOException {
        RequestBody requestBody = param.body();
        Request request = wrap(new Request.Builder()
                .url(param.url(context.sandbox()))
                .method(context.httpMethod(), requestBody))
                .build();
        return httpClient().newCall(request).execute();
    }

    public OkHttpClient httpClient() {
        return OK_HTTP_CLIENT;
    }
}
