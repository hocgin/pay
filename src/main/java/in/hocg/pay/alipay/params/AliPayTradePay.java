package in.hocg.pay.alipay.params;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import okhttp3.RequestBody;

@Data
@Builder
@Accessors(fluent = true)
public class AliPayTradePay extends AliPayParam {
    private String out_trade_no;
    private String scene;
    private String auth_code;
    private String product_code;
    private String subject;
    private String buyer_id;
    private String seller_id;
    private String total_amount;
    private String trans_currency;
    private String settle_currency;
    private String discountable_amount;
    private String body;
    private GoodsDetail[] goods_detail;

    @Data
    @Accessors(fluent = true)
    class GoodsDetail {
        private String goods_id;
        private String goods_name;
        private String quantity;
        private String price;
        private String goods_category;
        private String body;
        private String show_url;
    }
    private String operator_id;
    private String store_id;
    private String terminal_id;

    @Data
    @Accessors(fluent = true)
    class ExtendParams {
        private String sys_service_provider_id;
        private String industry_reflux_info;
        private String card_type;
    }

    private ExtendParams extend_params;
    private String timeout_express;
    private String auth_confirm_mode;
    private String terminal_params;


    @Override
    public RequestBody body() {
        return RequestBody.create(JSON, this.toString());
    }

}
