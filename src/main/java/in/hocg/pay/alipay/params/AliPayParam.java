package in.hocg.pay.alipay.params;

import in.hocg.pay.Param;

public abstract class AliPayParam implements Param {
    private final static String URL = "https://openapi.alipay.com/gateway.do";
    private final static String DEV_URL = "https://openapi.alipay.com/gateway.do";


    @Override
    public String url(boolean sandbox) {
        return sandbox ? DEV_URL : URL;
    }
}
