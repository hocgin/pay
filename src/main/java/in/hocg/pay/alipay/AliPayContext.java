package in.hocg.pay.alipay;

import in.hocg.pay.Context;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Optional;

@Data
@Accessors(fluent = true)
public class AliPayContext extends Context<AliPayContext, AliPayService> {
    private String app_id;
    private String method;
    private String charset = "utf-8";
    private String sign_type = "RSA2";
    private String sign;
    private String timestamp;
    private String version = "1.0";
    private String notify_url;
    private String app_auth_token;
    private String biz_content;

    private AliPayContext() {
    }

    public static AliPayContext ctx() {
        return new AliPayContext();
    }

    @Override
    public AliPayService getService() {
        Optional.of(app_id);
        Optional.of(method);
        Optional.of(charset);
        Optional.of(sign_type);
        Optional.of(sign);
        Optional.of(timestamp);
        Optional.of(version);
        Optional.of(biz_content);
        return new AliPayService(this);
    }

    @Override
    public AliPayContext xml() {
        throw new UnsupportedOperationException("仅支持JSON");
    }
}
