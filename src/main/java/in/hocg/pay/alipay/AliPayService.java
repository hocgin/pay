package in.hocg.pay.alipay;

import in.hocg.pay.Context;
import in.hocg.pay.Param;
import in.hocg.pay.Service;
import in.hocg.pay.alipay.params.AliPayTradePay;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.io.IOException;

public class AliPayService extends Service<AliPayContext> {

    public AliPayService(AliPayContext context) {
        super(context);
    }

    @Override
    protected Request.Builder wrap(Request.Builder builder) {
        return builder;
    }

    public String request(Param param) throws IOException {
        return call(param).body().string();
    }

}
